/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include <kpluginfactory.h>
#include <kpluginloader.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/iplugincontroller.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/idocument.h>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iprojectcontroller.h>
#include <interfaces/iproject.h>
#include <shell/problemmodelset.h>
#include <shell/problemmodel.h>
#include <shell/checkerstatus.h>
#include <language/interfaces/editorcontext.h>
#include <project/projectconfigpage.h>

#include <KLocalizedString>
#include <KActionCollection>

#include "plugin.h"

#include <QAction>
#include <QMessageBox>

#include "job.h"
#include "parser.h"

#include <QSet>

#include "preferencespage.h"
#include "configpage.h"

using namespace KDevelop;

K_PLUGIN_FACTORY_WITH_JSON(PylintFactory, "kdevpylint.json",  registerPlugin<Pylint::Plugin>();)

namespace Pylint
{

Plugin::Plugin(QObject *parent, const QVariantList&)
    : IPlugin("kdevpylint", parent)
    , m_model(new ProblemModel(parent))
    , m_status(new CheckerStatus())
{
    setXMLFile("kdevpylint.rc");
    m_status->setCheckerName("Pylint");

    createActions();

    ProblemModelSet *problemModelSet = core()->languageController()->problemModelSet();
    problemModelSet->addModel(QStringLiteral("Pylint"), m_model.data());

    core()->uiController()->registerStatus(m_status.data());
}

Plugin::~Plugin()
{
}

void Plugin::unload()
{
    ProblemModelSet *problemModelSet = core()->languageController()->problemModelSet();
    problemModelSet->removeModel(QStringLiteral("Pylint"));
}

ContextMenuExtension Plugin::contextMenuExtension(Context *context)
{
    ContextMenuExtension extension = IPlugin::contextMenuExtension(context);
    if (context->type() == Context::EditorContext) {
        extension.addAction(ContextMenuExtension::ExtensionGroup, createCheckFileAction());
        extension.addAction(ContextMenuExtension::ExtensionGroup, createCheckAllFilesAction());
    }

    return extension;
}

KDevelop::ConfigPage* Plugin::configPage(int number, QWidget *parent)
{
    if (number != 0)
        return nullptr;
    else
        return new PreferencesPage(this, parent);
}

KDevelop::ConfigPage* Plugin::perProjectConfigPage(int number, const ProjectConfigOptions &options, QWidget *parent)
{
    if (number != 0 )
        return nullptr;
    else
        return new ConfigPage(options.project, parent);
}

void Plugin::checkFile()
{
    check(false);
}

void Plugin::checkAllFiles()
{
    check(true);
}

void Plugin::onJobFinished(KJob *job)
{
    Job *checkJob = dynamic_cast<Job*>(job);
    if (checkJob == nullptr)
        return;

    if (checkJob->error() != KJob::NoError) {
        m_files.clear();

        switch (checkJob->error())
        {
            case Job::ProcessFailedToStart: {
                QMessageBox::critical(nullptr,
                                      i18n("Error starting Pylint"),
                                      i18n("Failed to start process. Queue cleared. Is the executable path correct?"));
                break;
            }

            case Job::ProcessCrashed: {
                QMessageBox::critical(nullptr,
                                      i18n("Error while running Pylint"),
                                      i18n("Process crashed. Queue cleared."));
                break;
            }

            case Job::UsageError: {
                QMessageBox::critical(nullptr,
                                  i18n("Error while running Pylint"),
                                  i18n("Process indicated there was a usage error. Queue cleared. Are the arguments correct?"));
                break;
            }

            case Job::UnknownError: {
                QMessageBox::critical(nullptr,
                                      i18n("Error while running Pylint"),
                                      i18n("Unknown error. Queue cleared."));
                break;
            }

        }

        return;
    }

    m_files.pop_front();

    Parser *parser = checkJob->parser();
    m_problems << parser->problems();

    m_status->itemChecked();

    /// If there are more files to check, check the next
    if (!m_files.isEmpty()) {
        checkNext();
        return;
    }

    m_status->stop();

    m_model->setProblems(m_problems);
    m_problems.clear();
}

bool hasExtension(const QString &file, const QStringList &extensionList)
{
    foreach (const QString &ext, extensionList) {
        if (file.endsWith(ext))
            return true;
    }

    return false;
}

void Plugin::check(bool allFiles)
{
    /// If there are still files queued for checking, refuse to start a new session
    if (!m_files.isEmpty()) {
        QMessageBox::critical(nullptr,
                              i18n("Error starting Pylint"),
                              i18n("There is already a check in progress."));
        return;
    }

    IDocument *doc = core()->documentController()->activeDocument();
    if (!doc) {
        QMessageBox::critical(nullptr,
                              i18n("Error starting Pylint"),
                              i18n("There are no files open."));
        return;
    }

    IProject *project = core()->projectController()->findProjectForUrl(doc->url());
    if (!project) {
        QMessageBox::critical(nullptr,
                              i18n("Error starting Pylint"),
                              i18n("Selected file is not in a project"));
        return;
    }

    KConfigGroup projectGroup = project->projectConfiguration()->group("Pylint");

    QString extensionField = projectGroup.readEntry("Extensions", QStringLiteral(".py"));
    QStringList extensions;
    if (!extensionField.isEmpty())
        extensions = extensionField.split(';');

    if (allFiles) {
        QSet<IndexedString> fileSet = project->fileSet();
        foreach (const IndexedString &indexedString, fileSet) {
            QString filePath = indexedString.toUrl().toLocalFile();

            /// Only add files with the right type
            if (hasExtension(filePath, extensions))
                m_files.push_back(filePath);

        }
    }
    else {
        QString filePath = doc->url().toLocalFile();
        if (hasExtension(filePath, extensions))
            m_files.push_back(filePath);
    }

    if (m_files.isEmpty()) {
        QMessageBox::critical(nullptr,
                              i18n("Error starting Pylint"),
                              i18n("There are no files in the check queue. Are the file extensions set correctly?"));
        return;
    }

    m_status->setMaxItems(m_files.count());
    m_status->start();

    /// Start the check!
    checkNext();
}

void Plugin::checkNext()
{
    Job *job = new Job(m_files.front(), this);
    connect(job, &KJob::finished, this, &Plugin::onJobFinished);
    core()->runController()->registerJob(job);
}

void Plugin::createActions()
{
    actionCollection()->addAction("pylint_file", createCheckFileAction());
    actionCollection()->addAction("pylint_all", createCheckAllFilesAction());
}

QAction* Plugin::createCheckFileAction()
{
    QAction *checkFileAction = new QAction(this);
    checkFileAction->setText(i18n("Pylint (file)"));
    checkFileAction->setStatusTip(i18n("Check the current file with Pylint"));
    connect(checkFileAction, &QAction::triggered, this, &Plugin::checkFile);

    return checkFileAction;
}

QAction* Plugin::createCheckAllFilesAction()
{
    QAction *checkAllFilesAction = new QAction(this);
    checkAllFilesAction->setText(i18n("Pylint (all files)"));
    checkAllFilesAction->setStatusTip(i18n("Check all project files with Pylint"));
    connect(checkAllFilesAction, &QAction::triggered, this, &Plugin::checkAllFiles);

    return checkAllFilesAction;
}


}

#include "plugin.moc"

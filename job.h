/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef PYLINT_JOB_H
#define PYLINT_JOB_H

#include <KJob>
#include <QString>
#include <QStringList>
#include <QProcess>

namespace Pylint
{

class Parser;

/// Background job that runs Pylint and parses the output
class Job : public KJob
{
    Q_OBJECT
public:
    const int usageErrorCode = 32;

    enum PylintJobError
    {
        ProcessFailedToStart = UserDefinedError + 1,
        ProcessCrashed,
        UsageError,
        UnknownError
    };

    explicit Job(const QString &path, QObject *parent = nullptr);
    ~Job();

    /// Starts the job
    virtual void start() override;

    Parser* parser() const;

protected:
    /// Kills the job
    virtual bool doKill() override;

private slots:
    /// triggered when the process stops with an error
    void onProcessError(QProcess::ProcessError error);

    /// triggered when the process finishes normally
    void onProcessFinished(int exitCode);

private:
    /// Clang-check parameters
    struct
    {
        QString executablePath;
        QString filePath;
        QString additionalArguments;
    }m_params;

    QScopedPointer<QProcess> m_process;
    QScopedPointer<Parser> m_parser;
};


}

#endif


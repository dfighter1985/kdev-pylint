/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef PYLINT_CONFIGPAGE_H
#define PYLINT_CONFIGPAGE_H

#include <interfaces/configpage.h>

namespace Ui
{
class ConfigPage;
}

namespace KDevelop
{
class IProject;
}

namespace Pylint
{

// Per project config page for Clang-check
class ConfigPage : public KDevelop::ConfigPage
{
    Q_OBJECT
public:
    ConfigPage(KDevelop::IProject *project, QWidget *parent);
    ~ConfigPage();

    virtual QString name() const override;

public slots:
    virtual void apply() override;
    virtual void defaults() override;
    virtual void reset() override;

private:
    QScopedPointer<Ui::ConfigPage> m_ui;
    KDevelop::IProject *m_project;

};

}

#endif


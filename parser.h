/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef PYLINT_PARSER_H
#define PYLINT_PARSER_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <shell/problem.h>

namespace Pylint
{


/**
 * @brief Parses the output of Pylint
 * started by:
 * pylint xyz.py
 */
class Parser
{
public:
    Parser();
    ~Parser();

    /// Sets the input for parsing (output of Clang-check)
    void setInput(const QString &in);

    /// Parse the input
    void parse();

    /// Returns the problems parsed
    QVector<KDevelop::IProblem::Ptr> problems() const { return m_problems; }

    /// Sets the path of the file whose issues are being parsed
    void setPath(const QString &path) { m_path = path; }

private:
    void parseLine(const QString &line);

    QString m_path;
    QStringList m_lines;
    QVector<KDevelop::IProblem::Ptr> m_problems;
};

}

#endif


"""This is a test file for kdev-pylint"""

def unused_argument(argument):
    """Triggers the unused argument message"""
    return

def unneccessary_semicolon():
    """Triggers the uneccessary semicolon message"""
    return;

def missing_function_docstring():
    return


def undefined_variable():
    """Triggers the undefined variable message"""
    var = a
    var = var + 1
    return

/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <QTest>
#include <QSignalSpy>

#include <KSharedConfig>
#include <KConfigGroup>

#include <kdevplatform/tests/testcore.h>
#include <kdevplatform/tests/autotestshell.h>
#include <kdevplatform/interfaces/icore.h>
#include <kdevplatform/interfaces/iruncontroller.h>


#include "../job.h"
#include "../parser.h"

using namespace KDevelop;
using namespace Pylint;

namespace
{

const QString fileName = QStringLiteral("examples/test.py");

struct ProblemData
{
    int line;
    int column;
    IProblem::Severity severity;
    QString msg;
};

const ProblemData testProblems[] = {
    { 9, 0, IProblem::Warning, QStringLiteral("Unnecessary semicolon (unnecessary-semicolon)") },
    { 3, 20, IProblem::Warning, QStringLiteral("Unused argument 'argument' (unused-argument)") },
    { 11, 0, IProblem::Hint, QStringLiteral("Missing function docstring (missing-docstring)") },
    { 17, 10, IProblem::Error, QStringLiteral("Undefined variable 'a' (undefined-variable)") },
};

#define MYCOMPARE(actual, expected) \
    if (!QTest::qCompare(actual, expected, #actual, #expected, __FILE__, __LINE__)) \
    return false

// Compares the result problems to the reference problems
bool checkProblems(const QVector<IProblem::Ptr> &problems)
{
    int c = sizeof(testProblems) / sizeof(ProblemData);

    MYCOMPARE(problems.count(), c);

    for (int i = 0; i < c; i++) {
        MYCOMPARE(problems[i]->finalLocation().document.str(), fileName);
        MYCOMPARE(problems[i]->finalLocation().start().line() + 1, testProblems[i].line);
        MYCOMPARE(problems[i]->finalLocation().start().column(), testProblems[i].column);
        MYCOMPARE(problems[i]->severity() == testProblems[i].severity, true);
        MYCOMPARE(problems[i]->description(), testProblems[i].msg);
    }

    return true;
}

}

class TestJob : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testJobWrongExecutable();
    void testJob();

private:
    QScopedPointer<Job> m_job;
};

void TestJob::initTestCase()
{
    AutoTestShell::init();
    TestCore::initialize(Core::NoUi);
}

void TestJob::cleanupTestCase()
{
    TestCore::shutdown();
}

void TestJob::testJobWrongExecutable()
{
    KConfigGroup group = KSharedConfig::openConfig()->group("Pylint");
    group.writeEntry(QStringLiteral("Pylint Path"), QStringLiteral("/usr/local/bin/yada/yada/wrong"));

    Job *job = new Job(fileName, this);
    QSignalSpy spy(job, &Job::finished);

    ICore::self()->runController()->registerJob(job);
    spy.wait();

    QVERIFY(job->error() != KJob::NoError);
    QVERIFY(job->error() == Job::ProcessFailedToStart);
}

void TestJob::testJob()
{
    KConfigGroup group = KSharedConfig::openConfig()->group("Pylint");
    group.writeEntry(QStringLiteral("Pylint Path"), QStringLiteral(""));

    Job *job = new Job(fileName, this);
    QSignalSpy spy(job, &Job::finished);

    ICore::self()->runController()->registerJob(job);
    spy.wait();

    QVERIFY(job->error() == KJob::NoError);

    QVector<IProblem::Ptr> problems = job->parser()->problems();
    QCOMPARE(problems.count(), 4);

    QVERIFY(checkProblems(problems));
}

QTEST_MAIN(TestJob)

#include "test_job.moc"
